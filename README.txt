Institut Pasteur
Walter and Eliza Hall Institut of Medical Research
Ivo Mueller

***********************************
** PV SEROTAT TOOL - README FILE **
***********************************

The Pv SeroTAT Tool has been developed by Ivo Mueller's research groups with major contributions by Connie 
Li Wai Suen and Michael White. Narimane Nekkab has assembled the Shiny App tool with the help of Thomas Obadia. 
Any troubleshooting questions can be sent to her at narimane.nekkab@pastuer.fr or michael.white@pasteur.fr 
or thomas.obadia@pasteur.fr

The Pv SeroTAT Shiny Tool folder should contain the following files and folders:
- RUN_PvSeroTAT: this is the main R script you should open
- SHINY_APP: this script contains the Shiny App functions
- FUNCTIONS: this script contains the analysis functions that are read into the app
- RESOURCES: this folder contains the default files loaded if the antigen names or plate template files are not loaded
- MODEL: this folder contains the models used to estimate seropositvity
- RESULTS: this is the default folder for storing outputs with experiment sub-folders created during the analysis (go to bottom to see description)
- Example data files: this folder contains example MFI, plate layout (.csv and .xlsx) and RAU files for data input and a PvSeroTAT output for reference.

***********
** SETUP **
***********

STEP 1: Install R and/or R Studio
- If the R program is not already installed on the machine, download it on this link:
https://cran.r-project.org/bin/windows/base/
- It is recommended to also install R Studio which provdes a more user-friendly interface:
https://rstudio.com/products/rstudio/download/

STEP 2: Open the RUN_PvSeroTAT.R file
- Click on the Run App button on the main console (next to a green arrow)
Note: internet access is not required to use this tool once R and/R Studio and the packages have been installed
- If an error is encountered and the interface does not open in a web browser, check the console in R Studio.
- In the console an error message may appear. Check that the packages have been properly installed.

****** Internet access not required for following steps: ******

********************
** ANALYSIS STEPS **
********************

STEP 3: Choose model
- Currently only the validated Random Forest Model is used in the app.
- The SVM model can be integrated by request
- It is important to determine if the RAMA protein corresponds to antigen W16 / L23 from CellFree Sciences or W47/PVX)087885 from Japan because the classification will not be the same
- The default option can be used is classification is not analyzed (see STEP 13 below)

STEP 4: Choose diagnostic target
- All three default targets dependong on the model can be used to classify sero-positivity
- If Other, give either the desired sensitivity or specificty value. The corresponding sensitivity or specificty value will be reported in the final results classification column header name.
i.e. If a sensitivity of 80% is chosen for the given model, the corresponding specificty will be given as: SEROPOSITIVE_80SE_60SP

STEP 5: Choose to load MagPix (Luminex machine) MFI results file OR a Relative Antibody Units (RAU) file 
- If loading MFI data, follow STEP 6 - STEP 9 then continue to STEP 13
- If loading a RAU file, skip to STEP 10

##################
### MFI UPLOAD ###

STEP 6: Load raw MFI file
- Load a CSV (.csv) or Excel (.xlsx) file
- For MFI data, data from lines 42 to 138 will be used. Please make sure that these lines contain the column header
(Location, Sample, Antigen names...)
- If loading a CSV (.csv) file, please indicate the number of antigens processed by the machine (required) i.e. number of columns
- Note that if the bead count is too low, the analysis will still run but you will receive a warning message.

STEP 7: Indicate the number of antigen proteins processed by the machine
- Give an integer between 0 to infinity
- It should correspond to the number of protein columns in the data file
i.e. 15

STEP 8: Load plate layout template file (if loading MFI data)
- Load a CSV (.csv) or Excel (.xlsx) file
- This file should contain the plate layout format. A default layout will be provided but should only be used as a reference.
- For each plate, a unique plate template shoud be loaded. The well names will be taken as bleedcodes and exported.
- This is essentiation for correctly identifying samples.

STEP 9: Load antigen name file (optional)
- If your antigen naming convention is not proposed in the default file, please load a file with the same format that matches the W names to your convention.
- If your antigen names are included in the default file, skip this step and do not a load a file.
- A default file will be used if left blank.

##################
### RAU UPLOAD ###

STEP 10: Load raw RAU file
- Load a CSV (.csv) or Excel (.xlsx) file
- If you are loading an RAU file that was exported by this app and not modified in any way, then proceed to STEP 13
- If you are loading an RAU file from another source, proceed to STEP 11

STEP 11: Indicate number of ID columns
- Input the number of columns that correspond to the ID columns starting from the left
- Example: if you have columns "Sample", "Barcode", "Location", input the number 3
- Do not skip columns
- These columns will be merged back after the analysis
- Double check that your data does not include NAs; remove this observations if data is missing

STEP 12: Indicate number of RAU columns
- Input the number of columns that correspond to the RAU columns starting from the left after the ID columns
- Example: if you have ID columns "Sample", "Barcode", "Location" followed by RAU columns "L01", "L02", "L23", then input 3
- Do not skip columns
- All antigens can be uploaded but make sure that the top 8 (in the default file) are included
- Double check that your data does not include NAs; remove this observations if data is missing

****************

STEP 13: Run only RAU conversion without classification
- Default is empty / No
- If "Yes" is selected, RAU conversion script will be run without classification. In this case, ignore model and diadnostic targets chosen (STEP 3 & 4)
- If all 8 antigens were not processed in the data, this options allowed for RAU conversion without errors.
- If left empty, all 8 antigens need to be in data file for classification

STEP 14: Change experiment output folder name (optional) ?
- A default experiment name using the MFI or RAU file name in /RESULTS
- If yes, input a file name without special characters that will be created in /RESULTS
- All outputs will be stored in this folder

STEP 15: Run Analysis
- Click on "Submit" once per uploaded file to run the analysis.
- Using the raw MFI data uploaded by the user, the tool will process a script to convert the MFI values to RAU.
- These values will be procced in an algorithm developed by our team to predict seropositivity.

***** Note that if all 8 protein antigens (refer to antigen name top 8 file) are not in the sample
or are not identified correctly due to their name, the analysis will fail ***** 

STEP 16: Status
- Once the MFI to RAU or RAU to PvSeroTAT analyses have finished, the experiment folder where the files are saved will be indicated.

****************

OUTPUT FILES:
- If sero-classification is run, there should be 7 output files
- If sero-classifcation is not run, there should be 6 output files
- ALL_RESULTS.RData file: all data used to generate plots and RAU outputs
- BLANK_SAMPLE_PLOT.pdf: MFI levels of each protein per blank sample
- MODEL_PLOTS.pdf: 5-parameter logistic standard curve  model per protein used to estimate relative antibody units from MFI
- PLATE_BEADS_COUNT_PLOT.pdf: indicates which samples have a low bead count and need to be re-run
- PVSEROTAT_RF_CLASS_RESULTS.csv: A (.csv) file of the relative antibody unit for the top 8 proteins used to determine seropositivity, 
the Random Forest prediction for classifying each sample, the seropostive status (sero-negative or sero-positive for Pv hypnozoites),
with the given sensitvity and specificty target used for the cut-off
- RAU_RESULTS.csv: A (.csv) file of the relative antibody unit for each protein, its reciprocal, the minimum and maximum standard, 
and the minimum and maximum dilution for that protein. 
- STD_CURVES_PLOT.pdf: log MFI distribution of standard curve per protein




